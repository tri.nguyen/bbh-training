
import numpy as np

import bilby
from bilby.gw.source import lal_binary_black_hole
from bilby.gw.conversion import convert_to_lal_binary_black_hole_parameters

from gwpy.timeseries import TimeSeries

### IO function
def dict2args(params, keys=None):
    """ Convert dictionary to commandline argument string """

    # If no key is given, take all keys
    if keys is None:
        keys  = params.keys()

    # Parse
    append = ''
    for key, val in params.items():
        if key not in keys:
            continue
        key = key.replace('_', '-')
        append += f'--{key} '
        if isinstance(val, (list, tuple)):
            for v in val:
                append += str(v)
                append += ' '
        elif val is not None:
            append += str(val)
            append += ' '
    append = append[:-1]  # remove the trailing white space
    return append

### Signal processing
def getSNR(data, noise_psd, fs, fmin=20):
    ''' Calculate the waveform SNR given the background noise PSD''' 
    L = len(data)
    
    data_fd = np.fft.rfft(data) / fs
    data_freq = np.fft.rfftfreq(L) * fs
    dfreq = data_freq[1] - data_freq[0]
    
    noise_psd_interp = noise_psd.interpolate(dfreq)
    noise_psd_interp[noise_psd_interp == 0] = 1.

    SNR = 4 * np.abs(data_fd)**2 / noise_psd_interp.value * dfreq    
    SNR = np.sum(SNR[fmin <= data_freq])
    SNR = np.sqrt(SNR)
    
    return SNR

def whiten(data, noise_psd, fs, fmin=20):
    ''' Whiten data given the background noise PSD '''
    L = len(data)
    
    dt = 1./fs
    data_fd = np.fft.rfft(data)
    data_freq = np.fft.rfftfreq(L, dt)
    dfreq = data_freq[1] - data_freq[0]
        
    noise_psd_interp = noise_psd.interpolate(dfreq)
    noise_psd_interp[noise_psd_interp == 0] = 1.
    
    norm = 1./np.sqrt(1./(dt * 2))
    whitened_data_fd = data_fd / np.sqrt(noise_psd_interp.value) * norm
    whitened_data = np.fft.irfft(whitened_data_fd, n=L)
    return whitened_data

def pearson_shift(x, y, shift):
    ''' Calculate the Pearson correlation coefficient between x and y
    for each array shift value from [0, shift]. 
    '''
    corr = []
    shift_arr = np.arange(-shift, shift)
    for s in shift_arr:
        x_roll = np.roll(x, s, axis=1)        
        mx = x_roll.mean(1, keepdims=True)
        my = y.mean(1, keepdims=True)
        xm, ym = x_roll - mx, y - my
        r_num = np.sum(xm * ym, axis=1)
        r_den = np.sqrt(np.sum(xm**2, axis=1) * np.sum(ym**2, axis=1))
        r = r_num / r_den
        corr.append(r)
    corr = np.stack(corr).T
    return corr

### Signal generation
def time_domain_SG(params, duration, sample_rate, clip=1.):
    ''' Function to generate Sine Gaussian'''
    # Get sine gauss parameters
    A = params.get('A', 1)
    f0 = params.get('f0', 100)
    tau = params.get('tau', 0.05)
    t0 = duration / 2.
    
    # Generate sine guassian
    t = np.arange(0., duration, 1./sample_rate)
    x = A * np.exp(-(t - t0)**2 / tau**2) * np.sin(2 * np.pi * f0 * (t - t0))
    
    # clip waveform 
    max_A = A * clip
    x[x > max_A] = max_A
    x[x < -max_A] = -max_A
    
    return x

def time_domain_blip(duration, sample_rate, f0_min=20, f0_width=1000, N=20):
    ''' Generate blip glitches from adding sine gaussian waveform '''
    f0_arr = 10**np.linspace(np.log10(f0_min), np.log10(f0_min + f0_width), N)
    tau_arr = 10**np.linspace(-1, -4, N)

    # Generate blip glitch by adding SG
    x = np.zeros(int(duration * sample_rate))
    for i in range(N):
        params = dict(f0=f0_arr[i], tau=tau_arr[i])
        x += time_domain_SG(params, duration, sample_rate)
    
    return x
        
# def ricker(duration, sample_rate, sigma):
#     ''' Generate a Ricker wavelet '''
    
#     t = np.arange(0., 2., 1./sample_rate)
#     t0 = duration / 2.
    
#     p = 2 / np.sqrt(3 * sigma * np.sqrt(np.pi))
#     p *= (1 - ((t-t0) / sigma)**2)
#     p *= np.exp(-(t - t0)**2 / 2 / sigma**2)
#     return p


def generateGW(sample_params, sample_duration, triggers, ifo, 
               waveform_generator=None, get_snr=False, noise_psd=None, 
               whiten_fn=None):
    ''' Generate gravitational-wave events
    
    Arguments:
    - sample_params: dictionary of GW parameters
    - sample_duration: time duration of each sample
    - triggers: trigger time (relative to `sample_duration`) of each sample
    - ifo: interferometer
    - waveform_generator: bilby.gw.WaveformGenerator with appropriate parameters
    - get_snr: return the SNR of each sample
    - noise_psd: background noise PSD used to calculate SNR or whiten the sample
    - whiten_fn: whiten each sample using the background noise PSD    
    '''
    
    
    N_sample = len(sample_params['geocent_time'])
    
    if waveform_generator is None:
        waveform_generator = bilby.gw.WaveformGenerator(
            duration=sample_duration * 2,
            sampling_frequency=16384,            
            frequency_domain_source_model=lal_binary_black_hole,
            parameter_conversion=convert_to_lal_binary_black_hole_parameters,
            waveform_arguments={
                'waveform_approximant': 'IMRPhenomPv2',
                'reference_frequency': 50,
                'minimum_frequency': 20 }
            ,
        )
    
    if isinstance(triggers, (int, float)):
        triggers = np.repeat(triggers, N_sample)
    
    sample_rate = waveform_generator.sampling_frequency
    sample_size = int(sample_rate * sample_duration)
    waveform_duration = waveform_generator.duration
    waveform_size = int(sample_rate * waveform_duration)
    
    signals = np.zeros((N_sample, sample_size))
    SNR = np.zeros(N_sample)
    
    ifo = bilby.gw.detector.get_empty_interferometer(ifo)
    for i in range(N_sample):
        # Get parameter for one signal
        p = dict()
        for k, v in sample_params.items():
            p[k] = v[i]
        ra, dec, geocent_time, psi = p['ra'], p['dec'], p['geocent_time'], p['psi']

        # Generate signal in IFO
        polarizations = waveform_generator.time_domain_strain(p)
        signal = np.zeros(waveform_size)
        for mode in polarizations.keys():
            # Get H1 response
            response = ifo.antenna_response(ra, dec, geocent_time, psi, mode)
            signal += polarizations[mode] * response
            
        # Total shift = shift to trigger time + geometric shift
        dt = (waveform_duration - sample_duration) / 2. + triggers[i] 
        dt += ifo.time_delay_from_geocenter(ra, dec, geocent_time)
        signal = np.roll(signal, int(np.round(dt*sample_rate)))

        # Calculate SNR and whiten signal
        if noise_psd is not None:
            if get_snr:
                SNR[i] = getSNR(signal, noise_psd, sample_rate)
            if whiten_fn is None:
                signal = TimeSeries(signal, dt=1./sample_rate).whiten(
                    asd=np.sqrt(noise_psd)).value
            else:
                signal = whiten_fn(signal, noise_psd, sample_rate)

        # Truncate signal
        idx_start = (waveform_size - sample_size) // 2
        idx_stop = idx_start + sample_size
        signals[i] = signal[idx_start: idx_stop]
    if get_snr:
        return signals, SNR
    return signals

def generateBlipGlitches(sample_params, sample_duration, triggers, 
                         sample_rate, waveform_duration=8, get_snr=False,  
                         noise_psd=None, whiten_fn=None):
    ''' Generate blip glitches event '''
    
    N_sample = len(sample_params['f0_min'])
    
    if isinstance(triggers, (int, float)):
        triggers = np.repeat(triggers, N_sample)
    
    sample_size = int(sample_duration * sample_rate)
    waveform_size = int(waveform_duration * sample_rate)
    
    signals = np.zeros((N_sample, sample_size))
    SNR = np.zeros(N_sample)
    for i in range(N_sample):
        
        f0_min = sample_params['f0_min'][i]
        f0_width = sample_params['f0_width'][i]

        signal = time_domain_blip(waveform_duration, sample_rate, f0_min, f0_width)
        
        # shift to trigger time
        dt = triggers[i] - sample_duration / 2.
        signal = np.roll(signal, int(dt * sample_rate))
        
        # Calculate SNR and whiten signal
        if noise_psd is not None:
            if get_snr:
                SNR[i] = getSNR(signal, noise_psd, sample_rate)
            if whiten_fn is None:
                signal = TimeSeries(signal, dt=1./sample_rate).whiten(
                    asd=np.sqrt(noise_psd)).value
            else:
                signal = whiten_fn(signal, noise_psd, sample_rate)

        # truncate signal
        idx_start = (waveform_size - sample_size) // 2
        idx_stop = idx_start + sample_size
        signals[i] = signal[idx_start: idx_stop]

    if get_snr:
        return signals, SNR
    return signals
