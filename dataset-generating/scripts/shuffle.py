
import os
import h5py
import glob
import argparse

import numpy as np

DATA_PARAMS_KEYS = (
    'f0_min', 'f0_width', 'a_1', 'a_2', 'dec', 'geocent_time', 'luminosity_distance', 
    'mass_1', 'mass_2', 'phase', 'phi_12', 'phi_jl', 'psi', 'ra', 'theta_jn', 
    'tilt_1', 'tilt_2', 'trigger-time')
FLAG_TO_LABEL = {'background': 0, 'gw': 1, 'glitch': 2}

# Parse arguments
def parse_args():
    parser = argparse.ArgumentParser()
    
    parser.add_argument('--indir', required=True)
    parser.add_argument('--outdir', required=True)
    parser.add_argument('--N-split', type=int, default=3)
    parser.add_argument('--no-shuffle', action='store_true')
    parser.add_argument('--seed', type=int)
    
    params = parser.parse_args()
    return params
cmd_params = parse_args()

np.random.seed(cmd_params.seed)

flist = sorted(glob.glob(os.path.join(cmd_params.indir, '*.h5')))
shuffle = not cmd_params.no_shuffle
os.makedirs(cmd_params.outdir, exist_ok=True)

# print(flist)

H1, L1, labels = [], [], []
params = {}
params['GPS-start'] = []
params['H1-SNR'] = []
params['L1-SNR'] = []
for key in DATA_PARAMS_KEYS:
    params[key] = []

for file in flist:
    with h5py.File(file, 'r') as f:
        flag = f.attrs['flag']
        size = f.attrs['size']
        label = FLAG_TO_LABEL[flag]
    
        print(flag, label)
    
        # Get data and labels
        H1.append(f['H1/timeseries'][:])
        L1.append(f['L1/timeseries'][:])
        labels.append(np.zeros(size) + label)
        
        # Get parameters
        nan_array = np.full(size, np.nan)
        params['GPS-start'].append(f['GPS-start'][:])

        if flag == 'background':
            params['H1-SNR'].append(nan_array)
            params['L1-SNR'].append(nan_array)
            for key in DATA_PARAMS_KEYS:
                params[key].append(nan_array)
                
        elif flag == 'gw' or flag == 'glitch':
            params['H1-SNR'].append(f['H1/SNR'][:])
            params['L1-SNR'].append(f['L1/SNR'][:])
            for key in DATA_PARAMS_KEYS:
                temp = np.array(f['signal_params'].get(key, nan_array))
                params[key].append(temp)

# concatenate dataset and shuffle data 
# concatenate
H1 = np.concatenate(H1)
L1 = np.concatenate(L1)
data = np.stack([H1, L1], axis=1)
labels = np.concatenate(labels).reshape(-1, 1)
for key, val in params.items():
    params[key] = np.concatenate(val)
    
# shuffle data 
if shuffle:
    indices = np.random.permutation(data.shape[0])
    data = data[indices]
    labels = labels[indices]
    for key, val in params.items():
        params[key] = val[indices]
else:
    indices = np.arange(data.shape[0])

# split into multiple files
data_split = np.array_split(data, cmd_params.N_split)
labels_split = np.array_split(labels, cmd_params.N_split)
params_split = {}
for key, val in params.items():
    params_split[key] = np.array_split(val, cmd_params.N_split)
indices_split = np.array_split(indices, cmd_params.N_split)

for i in range(cmd_params.N_split):
    # Save data
    out_fn = os.path.join(cmd_params.outdir, 'n{:02d}.h5'.format(i))
    print(out_fn)    
    x = data_split[i]
    y = labels_split[i]
    with h5py.File(out_fn, 'w') as f:
        f.attrs['size'] = len(x)
        f.create_dataset('data', data=x, chunks=True)
        f.create_dataset('label', data=y, chunks=True)
        
    # Save parameters
    out_params_fn = os.path.join(cmd_params.outdir, 'params-n{:02d}.h5'.format(i))
    print(out_params_fn)
    with h5py.File(out_params_fn, 'w') as f:
        f.attrs['size'] = len(indices_split[i])
        f.create_dataset('indices', data=indices_split[i], chunks=True)
        for key, val in params_split.items():
            f.create_dataset(key, data=val[i], chunks=True)
