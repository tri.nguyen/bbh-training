#!/usr/bin/env python
# coding: utf-8

import os
import sys
import h5py
import argparse 
import logging

import numpy as np
import scipy.signal as sig

import bilby
from bilby.gw.source import lal_binary_black_hole
from bilby.gw.conversion import convert_to_lal_binary_black_hole_parameters
from gwpy.timeseries import TimeSeries

import utils

logging.basicConfig(format='%(asctime)s - %(message)s', 
                    level=logging.INFO, stream=sys.stdout)

def getStrain(ifo, frame_start, frame_stop, sample_rate, fmin=None, shift=0.):
    ''' Get strain from [frame_start, frame_stop) from GWOSC, resample, 
    and apply high-pass filter '''
    strain = TimeSeries.fetch_open_data(ifo, frame_start + shift, frame_stop + shift)
    strain.t0 = strain.t0.unit * frame_start
    strain = strain.resample(sample_rate)
    
    if fmin is not None:
        strain = strain.highpass(fmin)
    return strain


# Parse cmd argument
def parse_cmd():
    parser = argparse.ArgumentParser()
    
    # GPS Time settings for strain and PSD strain
    parser.add_argument('--frame-start', type=float, required=True)
    parser.add_argument('--frame-stop', type=float, required=True)
    parser.add_argument('--psd-frame-start', type=float, required=True)
    parser.add_argument('--psd-frame-stop', type=float, required=True)
    parser.add_argument('--L1-shift', type=float, default=0.)
    
    # Input output settings
    parser.add_argument('--outfile', required=True)
    
    # Preprocess
    parser.add_argument('--fmin', type=float)
    
    # Simulations settings
    parser.add_argument('--signal-type', choices=('gw', 'glitch'), type=str.lower)
    parser.add_argument('--sample-duration', type=float, default=2.)
    parser.add_argument('--sample-rate', type=float, default=2048.)
    parser.add_argument('--dt', type=float, default=0.01953125)
    parser.add_argument('--min-trigger', type=float, default=1.73)
    parser.add_argument('--max-trigger', type=float, default=1.77)
    parser.add_argument('--prior-fn', default='config/priors/nonspin_BBH.prior')
    parser.add_argument('--seed', type=int)
    
    # Set some default
    params = parser.parse_args()
    return params
params = parse_cmd()

# Set parameters
fmin = params.fmin
sample_rate = params.sample_rate
sample_duration = params.sample_duration
sample_size = int(sample_rate * sample_duration)
step = int(sample_rate * params.dt)
fftlength = int(max(2, np.ceil(2048 / sample_rate)))

# Get strain and PSD strain from GWOSC
# Shift L1 GPS Time by `L1_shift`
frame_start = params.frame_start
frame_stop = params.frame_stop
psd_frame_start = params.psd_frame_start
psd_frame_stop = params.psd_frame_stop
L1_shift = params.L1_shift

logging.info('Download GW  strain from {} .. {}'.format(frame_start, frame_stop))
logging.info('Download PSD strain from {} .. {}'.format(psd_frame_start, psd_frame_stop))

H1_strain = getStrain('H1', frame_start, frame_stop, sample_rate, fmin)
L1_strain = getStrain('L1', frame_start, frame_stop, sample_rate, fmin, shift=L1_shift)
H1_psd_strain = getStrain('H1', psd_frame_start, psd_frame_stop, sample_rate, fmin)
L1_psd_strain = getStrain('L1', frame_start, frame_stop, sample_rate, fmin, shift=L1_shift)

# Calculate PSD and whiten noise
H1_psd = H1_psd_strain.psd(fftlength)
L1_psd = L1_psd_strain.psd(fftlength)
H1_strain_whiten = H1_strain.whiten(asd=np.sqrt(H1_psd))
L1_strain_whiten = L1_strain.whiten(asd=np.sqrt(L1_psd))
# crop out corrupted data
H1_strain_whiten = H1_strain_whiten.crop(frame_start + 4, frame_stop - 4)
L1_strain_whiten = L1_strain_whiten.crop(frame_start + 4, frame_stop - 4)

N_sample = int((len(H1_strain_whiten) - sample_size) / step)
gps_start = np.arange(
    frame_start + 4, frame_start + 4 + params.dt * N_sample , params.dt)

logging.info('Number of samples: {}'.format(N_sample))

# Set up signal parameters
# For GW signal
if params.signal_type == 'gw':
    logging.info('Generating GW signals')
    
    # define waveform generator
    waveform_duration = 8
    waveform_arguments = {
        'waveform_approximant': 'IMRPhenomPv2',
        'reference_frequency': 50,
        'minimum_frequency': 20 }
    waveform_generator = bilby.gw.WaveformGenerator(
        duration=waveform_duration,
        sampling_frequency=sample_rate,
        frequency_domain_source_model=lal_binary_black_hole,
        parameter_conversion=convert_to_lal_binary_black_hole_parameters,
        waveform_arguments=waveform_arguments,
    )
    
    # sample GW parameters from prior distribution    
    priors = bilby.gw.prior.BBHPriorDict(params.prior_fn)
    sample_params = priors.sample(N_sample)
    
    triggers = np.random.uniform(params.min_trigger, params.max_trigger, N_sample)
    geocent_time = triggers + gps_start
    sample_params['geocent_time'] = geocent_time
        
    # generate whitened GW waveforms
    H1_signals, H1_SNR = utils.generateGW(
        sample_params, sample_duration, triggers, 'H1',
        waveform_generator=waveform_generator, get_snr=True,
        noise_psd=H1_psd)
    L1_signals, L1_SNR = utils.generateGW(
        sample_params, sample_duration, triggers, 'L1',
        waveform_generator=waveform_generator, get_snr=True,
        noise_psd=L1_psd)

# For blip glitch
elif params.signal_type == 'glitch':
    logging.info('Generating blip glitch signals')
    
    waveform_duration = 8
    
    # sample blip glitches parameters
    priors = bilby.prior.PriorDict(params.prior_fn)
    N_sample_H1 = N_sample // 2
    N_sample_L1 = N_sample - N_sample // 2
    H1_params = priors.sample(N_sample_H1)    
    L1_params = priors.sample(N_sample_L1)
    sample_params = {'f0_min': [], 'f0_width': [], 'SNR': []}
    for key in sample_params.keys():
        sample_params[key] = np.concatenate([H1_params[key], L1_params[key]])
    sample_params['ifo'] = np.concatenate([np.zeros(N_sample_H1, dtype=int), 
                                           np.ones(N_sample_L1, dtype=int)])

    # sample trigger time
    H1_triggers = np.random.uniform(params.min_trigger, params.max_trigger, N_sample_H1)
    L1_triggers = np.random.uniform(params.min_trigger, params.max_trigger, N_sample_L1)
    triggers = np.concatenate([H1_triggers, L1_triggers])

    # generate H1 glitches
    H1_signals_temp, H1_SNR_temp = utils.generateBlipGlitches(
        H1_params, sample_duration, H1_triggers, sample_rate,
        waveform_duration=waveform_duration, get_snr=True, noise_psd=H1_psd
    )
    # generate L1 glitches
    L1_signals_temp, L1_SNR_temp = utils.generateBlipGlitches(
        L1_params, sample_duration, L1_triggers, sample_rate,
        waveform_duration=waveform_duration, get_snr=True, noise_psd=L1_psd
    )
    
    # rescale SNR
    for i in range(N_sample_H1):
        new_SNR = H1_params['SNR'][i]
        H1_signals_temp[i] *= new_SNR / H1_SNR_temp[i]
        H1_SNR_temp[i] = new_SNR
    for i in range(N_sample_L1):
        new_SNR = L1_params['SNR'][i]
        L1_signals_temp[i] *= new_SNR / L1_SNR_temp[i]
        L1_SNR_temp[i] = new_SNR
    
    # concatenate into arrays
    H1_signals = np.concatenate([H1_signals_temp, np.zeros_like(L1_signals_temp)])
    L1_signals = np.concatenate([np.zeros_like(H1_signals_temp), L1_signals_temp])
    H1_SNR = np.concatenate([H1_SNR_temp, np.zeros_like(L1_SNR_temp)])
    L1_SNR = np.concatenate([np.zeros_like(H1_SNR_temp), L1_SNR_temp])
    
    # shuffle
    idx_shuffle = np.random.permutation(N_sample)
    H1_signals = H1_signals[idx_shuffle]
    L1_signals = L1_signals[idx_shuffle]
    H1_SNR = H1_SNR[idx_shuffle]
    L1_SNR = L1_SNR[idx_shuffle]
    triggers = triggers[idx_shuffle]
    for key, val in sample_params.items():
        sample_params[key] = val[idx_shuffle]
    
# Sample noise and add GW signal
H1_data = np.zeros((N_sample, sample_size))
L1_data = np.zeros((N_sample, sample_size))
for i in range(N_sample):
    # get noise from whitened strain
    idx_start = i * step
    idx_stop = idx_start + sample_size
    H1_noise = H1_strain_whiten[idx_start: idx_stop]
    L1_noise = L1_strain_whiten[idx_start: idx_stop]
    
    # add whitened signal to whitened noise
    if params.signal_type is not None:
        H1_signal = H1_signals[i]
        L1_signal = L1_signals[i]    
        H1_data[i] = H1_signal + H1_noise
        L1_data[i] = L1_signal + L1_noise
    else:
        H1_data[i] = H1_noise
        L1_data[i] = L1_noise

# Write to output file
logging.info('Write to file {}'.format(params.outfile))
with h5py.File(params.outfile, 'w') as f:
    # write data and ASD
    # Hanford
    H1_gr = f.create_group('H1')
    H1_dset = H1_gr.create_dataset('timeseries', data=H1_data)
    H1_dset.attrs['channel'] = 'GWOSC'
    H1_gr.create_dataset('PSD', data=H1_psd.value)
    H1_gr.create_dataset('freq', data=H1_psd.frequencies.value)

    # Livingston
    L1_gr = f.create_group('L1')
    L1_dset = L1_gr.create_dataset('timeseries', data=L1_data)
    L1_dset.attrs['channel'] = 'GWOSC'
    L1_gr.create_dataset('PSD', data=L1_psd.value)
    L1_gr.create_dataset('freq', data=L1_psd.frequencies.value)

    f.create_dataset('GPS-start', data=gps_start)

    # write noise attributes
    f.attrs.update({
        'size': N_sample,
        'frame_start': frame_start,
        'frame_stop': frame_stop,
        'psd_frame_start': psd_frame_start,
        'psd_frame_stop': psd_frame_stop,
        'sample_rate': sample_rate,
        'sample_duration': sample_duration,
        'psd_fftlength': fftlength,
        'L1_shift': L1_shift,
    })
    if params.signal_type is None:
        f.attrs['flag'] = 'background'
    else:
        f.attrs['flag'] = params.signal_type
    
    # write signals attributes, SNR, and signal parameters
    if params.signal_type is not None:
        H1_gr.create_dataset('signal', data=H1_signals)
        L1_gr.create_dataset('signal', data=L1_signals)

        H1_gr.create_dataset('SNR', data=H1_SNR)
        L1_gr.create_dataset('SNR', data=L1_SNR)
        params_gr = f.create_group('signal_params')
        for k, v in sample_params.items():
            params_gr.create_dataset(k, data=v)
        params_gr.create_dataset('trigger-time', data=triggers) 

        # Update signal attributes
        f.attrs.update({'waveform_duration': waveform_duration})
