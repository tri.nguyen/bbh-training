#!/usr/bin/env python
# coding: utf-8

import os
import h5py
import argparse

import numpy as np
import scipy.signal as sig

import torch

# Define parameters
fmin = 20
fmax = 990
alpha = 0.2
scale = 1
high = 1000

# phase filtering function
def phase_filter(data, fs, fmin, fmax, alpha=0.):
    ''' Phase filter 
    
    Parameters:
    - data
    - fs: sampling frequency in Hz
    - fmin, fmax: frequency band
    
    Returns:
    - data_filt: filtered data
    '''
    bb, ab = sig.butter(4, [fmin*2./fs, fmax*2./fs], btype='band')
    normalization = np.sqrt((fmax-fmin)/(fs/2))
    data_filt = sig.filtfilt(bb, ab, data)/normalization
    # apply smoothing window
    data_filt *= sig.tukey(data_filt.shape[1], alpha=alpha)  
    return data_filt

# crop function
def crop(data, high, low=None):
    if low is None:
        low = -high
    data[data > high] = high
    data[data < low] = low 
    return data

def parse_cmd():
    parser = argparse.ArgumentParser()
    parser.add_argument('--filename', required=True)
    parser.add_argument('--outdir', required=True)
    parser.add_argument('--label', type=int, required=True)
    parser.add_argument('--signal', action='store_true')
    parser.add_argument('--skip-preprocess', action='store_true')
    params = parser.parse_args()
    return params
params = parse_cmd()

os.makedirs(params.outdir, exist_ok=True)

print('Applying phase filter: {} Hz to {} Hz'.format(fmin, fmax))
print('Applying Tukey window with alpha: {}'.format(alpha))
with h5py.File(params.filename, 'r') as f:
    H1_data = f['H1/timeseries'][:]
    L1_data = f['L1/timeseries'][:]
    gps_start = f['GPS-start'][:]
    sample_rate = f.attrs['sample_rate']
    
    # preprocess function
    def preprocess_fn(data):
        # phase filter, rescale and crop
        data = phase_filter(data, sample_rate, fmin, fmax, alpha)
        data = data / scale
        data = crop(data, high)
        return data    

    if not params.skip_preprocess:
        H1_data = preprocess_fn(H1_data)
        L1_data = preprocess_fn(L1_data)
    data = np.stack([H1_data, L1_data], axis=1)

    if params.signal:
        H1_SNR = f['H1/SNR'][:]
        L1_SNR = f['L1/SNR'][:]
        SNR = np.sqrt(H1_SNR**2 + L1_SNR**2)
        signal_params = {}
        for k, v in f['signal_params'].items():
            signal_params[k] = v[:]
            
    # Write output
    print('Saving output to directory: {}'.format(params.outdir))
    for i in range(len(data)):
        savepath = os.path.join(params.outdir, f'data_{i}.pt')

        # Create save object
        # write time series and label
        savedict = {'x': data[i], 'y': [params.label]}
        savedict['GPS-start'] = gps_start[i]
        if params.signal:
            savedict['H1-SNR'] = H1_SNR[i]
            savedict['L1-SNR'] = L1_SNR[i]
            for k, v in signal_params.items():
                savedict[k] = v[i]

        # write to file
        torch.save(savedict, savepath)
