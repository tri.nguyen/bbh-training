#!/usr/bin/env python
# coding: utf-8

import os
import glob
import argparse
import h5py

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

mpl.rc('font', size=15)
mpl.rc('figure', figsize=(8, 5), facecolor='w')
mpl.rcParams['agg.path.chunksize'] = 10000

from gwpy.timeseries import TimeSeries
from gwpy.frequencyseries import FrequencySeries

def plot_PSD(freq, H1_psd, L1_psd, output=None):
    fig, ax = plt.subplots(1, figsize=(8,5))

    ax.loglog(freq, H1_psd, label='H1')
    ax.loglog(freq, L1_psd, label='L1')

    ax.set_xlabel('Frequency [Hz]')
    ax.set_ylabel('Strain PSD')
    ax.set_xlim(5, 1100)
    ax.set_title('GPS {:d} - {:d}'.format(int(t_start), int(dt)))
    ax.legend(loc=1)

    fig.tight_layout()
    if output is not None:
        fig.savefig(output, dpi=300)
    return fig, ax

def plot_STRAIN(H1_whiten, L1_whiten, output=None):
    fig, ax = plt.subplots(1, figsize=(12, 5))

    ax.plot(H1_whiten, label='H1')
    ax.plot(L1_whiten, label='L1')

    ax.set_xlabel('GPS [sec]')
    ax.set_ylabel('Whitened Strain')
    ax.set_ylim(-10, 10)
    ax.set_title('GPS {:d} - {:d}'.format(int(t_start), int(dt)))
    ax.legend(loc=1)

    fig.tight_layout()
    if output is not None:
        fig.savefig(output, dpi=300)
    return fig, ax

# Parse CMD
def parse_cmd():
    parser = argparse.ArgumentParser()
    parser.add_argument('--dataset-dir')
    parser.add_argument('--prefix')
    parser.add_argument('--N-classes', type=int, default=3)
    parser.add_argument('--N-per-classes', type=int, default=1000)
    params = parser.parse_args()
    return params
params = parse_cmd()

N_classes = params.N_classes
N_per_classes = params.N_per_classes
dataset_dir = params.dataset_dir
prefix = params.prefix

plot_dir = os.path.join(dataset_dir, 'plots')
psd_dir = os.path.join(plot_dir, 'PSD')
strain_dir = os.path.join(plot_dir, 'STRAIN')

os.makedirs(plot_dir, exist_ok=True)
os.makedirs(psd_dir, exist_ok=True)
os.makedirs(strain_dir, exist_ok=True)

for i in range(N_classes):
    for j in range(N_per_classes):
        fn = os.path.join(dataset_dir, '{}_C{:02d}_{:02d}.h5'.format(prefix, i, j))        
        if not os.path.exists(fn):
            fn = os.path.join(dataset_dir, '{}_C{:02d}_{:d}.h5'.format(prefix, i, j))        
            if not os.path.exists(fn):
                break
        print(fn)
        
        # Read file
        with h5py.File(fn, 'r') as f:
            meta = dict(f.attrs)
            try:
                H1_psd = f['H1/PSD'][:]
                L1_psd = f['L1/PSD'][:]
                freq = f['H1/freq'][:]
            except:
                H1_psd = f['H1/ASD'][:]**2
                L1_psd = f['L1/ASD'][:]**2
                freq = f['H1/freq'][:]

        # Plot PSD
        t_start = meta['psd_frame_start']
        t_stop = meta['psd_frame_stop']
        dt = t_stop - t_start

        output = os.path.join(psd_dir, 'C{:02d}_{:02d}.png'.format(i, j))
        fig, ax = plot_PSD(freq, H1_psd, L1_psd, output)
        plt.close()
        
        # Plot strain
        t_start = meta['frame_start']
        t_stop = meta['frame_stop']
        dt = t_stop - t_start
        sample_rate = meta['sample_rate']
        H1 = TimeSeries.fetch_open_data('H1', t_start, t_stop)
        L1 = TimeSeries.fetch_open_data('L1', t_start, t_stop)
        H1 = H1.resample(sample_rate)
        L1 = L1.resample(sample_rate)
        H1_psd = FrequencySeries(H1_psd, df=freq[1]-freq[0])
        L1_psd = FrequencySeries(L1_psd, df=freq[1]-freq[0])
        H1_whiten = H1.whiten(asd=np.sqrt(H1_psd))
        L1_whiten = L1.whiten(asd=np.sqrt(L1_psd))
        
        output = os.path.join(strain_dir, 'C{:02d}_{:02d}.png'.format(i, j))
        plot_STRAIN(H1_whiten, L1_whiten, output)
        plt.close()
