#!/usr/bin/env python
# coding: utf-8

import os
import glob
import h5py
import argparse
import logging

import numpy as np

import utils

logging.basicConfig(format='%(asctime)s - %(message)s', 
                    level=logging.INFO, stream=sys.stdout)
def parse_cmd():
    parser = ArgumentParser()
    parser.add_argument('--in-dir')
    parser.add_argument('--shift', type=int)
    parser.add_argument('--sample-rate', type=float, default=1024)
    parser.add_argument('--t-start', type=float, default=0)
    parser.add_argument('--key', default='corr')
    params = parser.parse_args()
    return params
params = parse_cmd()

shift = params.shift
idx_start = int(params.sample_rate * params.t_start)
key = params.key

for i in range(10000):
    fn = os.path.join(params.in_dir, 'n{:02d}'.format(i))
    print(fn)
    if not os.path.exists(fn):
        break    
    try:
        with h5py.File(fn, 'r') as f:
            data = f['data'][:]
            x = data[:, 0, idx_start:]
            y = data[:, 1, idx_start:]
        corr = utils.pearson_shift(x, y, shift)
            
        with h5py.File(fn, 'a') as f:
            if f.get(key) is None:
                print('Creating new dataset')
                f.create_dataset(key, data=corr, chunks=True)
            else:
                print('Overwriting existing dataset')
                del f[key]
                f.create_dataset(key, data=corr, chunks=True)
    except Exception as e:
        print(e)
