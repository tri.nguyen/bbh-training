#!/usr/bin/env python
# coding: utf-8

import os
import argparse

import numpy as np

from gwpy.segments import DataQualityFlag

# Parse arguments from command line
def parse_cmd():
    parser = argparse.ArgumentParser()
    
    parser.add_argument('--gps-start',help='Start time of segments', nargs='+', type=int)
    parser.add_argument('--gps-end', help='End time of segments', nargs='+', type=int)
    parser.add_argument('--ifo', help='Inteferometer', default='H1L1')
    parser.add_argument('--segment-max-duration', help='Max duration of each segment', 
                        default=4096, type=int)
    parser.add_argument('--exclude-gps', help='Exclude GPS Time', default=(), 
                        type=float, nargs='+')
    parser.add_argument('--out-file', help='Output file', type=str)
    params = parser.parse_args()
    return params

params = parse_cmd()


start, end, duration = [], [], []
for gps_start, gps_end in zip(params.gps_start, params.gps_end):

    if params.ifo == 'H1L1':
        H1_segments = DataQualityFlag.query(
            'H1:DMT-ANALYSIS_READY:1', gps_start, gps_end)
        L1_segments = DataQualityFlag.query(
            'L1:DMT-ANALYSIS_READY:1', gps_start, gps_end)
        segments = H1_segments & L1_segments
    elif params.ifo == 'H1':
        segments = DataQualityFlag.query(
            'H1:DMT-ANALYSIS_READY:1', gps_start, gps_end)
    elif params.ifo == 'L1':
        segments = DataQualityFlag.query(
            'L1:DMT-ANALYSIS_READY:1', gps_start, gps_end)

    # Get segments
    start_temp, end_temp, duration_temp = [], [], []
    for seg in segments.active:
        seg_start = np.arange(seg.start, seg.end, params.segment_max_duration)
        seg_end = seg_start + params.segment_max_duration
        seg_end[-1] = int(seg.end)


        for i in range(len(seg_start)):
            add = True
            for exclude_gps in params.exclude_gps:
                if (seg_start[i] < exclude_gps) and (exclude_gps < seg_end[i]):
                    add = False
                    print('Ignoring {} .. {}: coincide with {}'.format(
                        seg_start[i], seg_end[i], exclude_gps))
                    break
            if add:
                start_temp.append(seg_start[i])
                end_temp.append(seg_end[i])
                duration_temp.append(seg_end[i] - seg_start[i])

    start.append(start_temp)
    end.append(end_temp)
    duration.append(duration_temp)
                
start = np.concatenate(start)
end = np.concatenate(end)
duration = np.concatenate(duration)
        
# Save segments    
table = np.array([start, end, duration]).T
header = 'start     end     duration'
if params.out_file is None:
    params.out_file = f'{params.ifo}_segments.txt'
np.savetxt(params.out_file, table, header=header, fmt='%d')
