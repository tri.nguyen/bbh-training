
import os
import errno
import shutil
import sys

import numpy as np

from pycondor import Job, Dagman

prefix = 'TrGW170104'
train_set = '/home/tri.nguyen/bbh/bbh_classification/data/dataset/TrGW170104'
device = 'cuda'
model = 1
base_out_dir = '/home/tri.nguyen/bbh/bbh_classification/bbh-training/data/TrGW170104-CNNsmall/01'

if not os.path.exists(train_set):
    raise FileNotFoundError(
        errno.ENOENT, os.strerror(errno.ENOENT), train_set)
    
if os.path.exists(base_out_dir):
    rep = input('File already existed. Continue [y/n]?')
    if rep != 'y':
        print('Bye')
        sys.exit(1)
    
dagman_name = prefix
condor_dir = os.path.join(base_out_dir, 'condor')
os.makedirs(base_out_dir, exist_ok=True)
os.makedirs(condor_dir, exist_ok=True)

# Define HTCondor parameters
job_args = dict(
    submit=os.path.join(condor_dir, 'submit'),
    log=os.path.join(condor_dir, 'log'),
    error=os.path.join(condor_dir, 'error'),
    output=os.path.join(condor_dir, 'output'),
    getenv=True,
    universe='vanilla',
    requirements='(CUDACapability > 3.5)',
    notification=None,
    request_memory='2 GB',
    extra_lines= [
        'accounting_group = ligo.dev.o3.cbc.bbh.pycbcoffline',
        'request_gpus = 1',
        'stream_output = True',
        'stream_error = True']
)

# Define Dagman
dagman = Dagman(name=dagman_name, 
                submit=os.path.join(condor_dir, 'submit'))
 
# Add job to Dagman
# Add CNN job
out_dir = os.path.join(base_out_dir, 'CNN')
cnn_args = '--train-set {} --outdir {} --model {} --shuffle'.format(
    train_set, out_dir, model)
cnn_job = Job(name='TrainCNN', dag=dagman, executable='./train.py',
              arguments=[cnn_args], **job_args)

# Add H1, L1 and ENN job
out_dir = os.path.join(base_out_dir, 'ENN')
H1_dir = os.path.join(out_dir, 'H1')
L1_dir = os.path.join(out_dir, 'L1')
H1_args = '--train-set {} --outdir {} --model {} --ifo H1 --shuffle'.format(
    train_set, H1_dir, model)
L1_args = '--train-set {} --outdir {} --model {} --ifo L1 --shuffle'.format(
    train_set, L1_dir, model)
enn_args = '--train-set {} --outdir {} --H1-dir {} --L1-dir {} --model {} --shuffle'.format(
    train_set, out_dir, os.path.join(H1_dir, 'models'), os.path.join(L1_dir, 'models'), model)

H1_job = Job(name='TrainH1', dag=dagman, executable='./train.py',
              arguments=[H1_args], **job_args)
L1_job = Job(name='TrainL1', dag=dagman, executable='./train.py',
              arguments=[L1_args], **job_args)
enn_job = Job(name='TrainENN', dag=dagman, executable='./train_ensemble.py', 
              arguments=[enn_args], **job_args)
enn_job.add_parent(H1_job)
enn_job.add_parent(L1_job)

# Submit DAGMAN        
dagman.build_submit()
dagman.visualize(os.path.join(condor_dir, 'workflow.png'))
