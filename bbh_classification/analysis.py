
import numpy as np

# Function to calculate ROC curve
def getROC(s_signal, s_noise, threshold):
    tp = np.sum(np.where(s_signal.reshape(-1, 1) > threshold, 1., 0.), 0) 
    tp /= len(s_signal)
    fp = np.sum(np.where(s_noise.reshape(-1, 1) > threshold, 1., 0.), 0) 
    fp /= len(s_noise)
    return tp, fp

# Function to calculate AUC of ROC curve
def getAUC(tp, fp):
    ''' Function to calculate AUC given true postitive and false positive '''
    # First, we have to sort by order of increasing false positive
    mask = np.argsort(fp)
    false_pos = fp[mask]
    true_pos = tp[mask]
    
    # Calculating AUC by trapezoidal method 
    dx = false_pos[1:] - false_pos[:-1]
    y = (true_pos[1:] + true_pos[:-1]) / 2.
    return np.sum(y * dx)