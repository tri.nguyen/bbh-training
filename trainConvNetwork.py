#!/usr/bin/env python
# coding: utf-8

import os
import sys
import argparse
import logging

import numpy as np
import scipy.signal as sig
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader

from gwpy.timeseries import TimeSeries

from bbh_classification import classifiers, utils, data_utils
from bbh_classification.logger import logger

logging.basicConfig(format='%(asctime)s - %(message)s', 
                    level=logging.DEBUG, stream=sys.stdout)

def parse_cmd():
    parser = argparse.ArgumentParser()

    # input and output arguments
    parser.add_argument('--train-set', required=True, 
                        help='Path to directory with training data')    
    parser.add_argument('--val-set',
                        help='Path to directory with validation data')
    parser.add_argument('--outdir', required=True, help='Path to output directory')    
    parser.add_argument('--store-val-output', action='store_true', 
                        help='Enable to store validation data NN output to outdir')

    # classifier arguments    
    parser.add_argument('--arch', choices=(classifiers.ALL_NAMES.keys()), 
                        type=str.upper, required=True, help='NN architecture to use')
    parser.add_argument('--input-shape', nargs='+', type=int, default=(2, 1024),
                        help='Input dimension for NN')
    parser.add_argument('--num-classes', type=int, default=2, help='Number of classes')
    parser.add_argument('--padding', type=int, default=1, help='Padding for Conv layers') 
    parser.add_argument('--corr-dim', type=int, default=0,
                        help='Dimension of correlation array. If 0, correlation will not be used.')
    parser.add_argument('--resume', action='store_true',
                        help='Resume training using the latest models available')
    
    # training arguments
    parser.add_argument('--max-epochs', type=int, default=15, help='Maximum number of epochs')
    parser.add_argument('--batch-size', type=int, default=128, help='Batch size')
    parser.add_argument('--lr', type=float, default=1e-3, help='Learning rate')
    parser.add_argument('--weight-decay', type=float, default=1e-3, help='L1 regularization')
    parser.add_argument('--shuffle', action='store_true', help='Enable shuffling training data')
    parser.add_argument('--loss-weight', nargs='+', type=float, help='Loss function weight of each class')
    
    # CUDA arguments
    parser.add_argument('--device', default='cuda', help='Name of devices to use')

    params = parser.parse_args()
    if params.val_set is None:
        params.val_set = os.path.join(params.train_set, 'val')
    return params
params = parse_cmd()

# get CUDA device
device = utils.get_device(params.device)

# initialize Dataset and Dataloader objects
keys = ['data', 'label']
if params.corr_dim > 0:
    keys.append('corr')
train_dataset = data_utils.Dataset(params.train_set, keys, params.shuffle)
val_dataset = data_utils.Dataset(params.val_set, keys)

# data is loaded on a CPU, pin_memory speeds up device transfer to GPU
if params.device == 'cpu':
    pin_memory = False
else:
    pin_memory = True
train_loader = DataLoader(train_dataset, batch_size=params.batch_size, num_workers=4, 
                          pin_memory=pin_memory, drop_last=True)
val_loader = DataLoader(val_dataset, batch_size=params.batch_size, num_workers=4, 
                        pin_memory=pin_memory)

logging.info('Number of training samples  : {} '.format(len(train_dataset)))
logging.info('Number of validation samples: {} '.format(len(val_dataset)))

# initialize NN model
arch = classifiers.get_arch(params.arch)
net = arch.Classifier(params.input_shape, params.num_classes, params.corr_dim, params.padding)
net = net.to(device)
if params.resume:
    state = utils.get_checkpoint(os.path.join(params.outdir, 'models'))
    logging.info('Resume training from state: {}'.format(state))
    net.load_state_dict(torch.load(state, map_location=device))
    
logging.info('Number of classes: {}'.format(params.num_classes))
logging.info('Correlation array dim:  {}'.format(params.corr_dim))
logging.info('Loss weight: {}'.format(params.loss_weight))
             
# define loss function, optimizer, LR scheduler, and training logger
if params.num_classes == 2:
    criterion = nn.BCEWithLogitsLoss(weight=params.loss_weight, reduction='sum')
else:
    criterion = nn.CrossEntropyLoss(weight=params.loss_weight, reduction='sum')
optimizer = optim.Adam(net.parameters(), lr=params.lr, weight_decay=params.weight_decay)
scheduler = optim.lr_scheduler.StepLR(optimizer, 5, 0.5)
train_log = logger.ClassifierLogger(outdir=params.outdir, metrics=['loss'])

# start training
np.random.seed()  # reset seed
n_batch_total = len(train_loader)

utils.print_train()
for epoch in range(params.max_epochs):
    # training
    net.train()  # switch to train mode
    train_loss = 0.
    for i, datab in enumerate(train_loader):
        optimizer.zero_grad()  # reset gradient
        
        # if use correlation array
        if params.corr_dim > 0:
            xb, yb, xcorrb = datab
            xcorrb = xcorrb.float().to(device)
        # if not use correlation array
        else:
            xb, yb = datab
            xcorrb = None
            
        # type conversion and move to GPU
        xb = xb.float().to(device)
        if params.num_classes == 2:
            yb = data_utils.label_mixing(yb, 2, 0)  # mix noise (0) and glitch (2) label into (0)
            yb = yb.float().to(device)   # BCELoss takes float tensor
        else:
            yb = yb.view(-1).long().to(device)    # CrossEntropyLoss takes long tensor
        
        # forward pass, calculate loss, backward pass, and GD
        yhatb = net(xb, xcorrb)
        loss = criterion(yhatb, yb)
        loss.backward()
        optimizer.step()
        train_loss += loss.item()    # update training loss
    scheduler.step()  # update LR scheduler
     
    # validating
    net.eval()    # switch to eval mode
    val_loss = 0.
    with torch.no_grad():   # disable GD to save computation
        for i, datab in enumerate(val_loader): 
            # if use correlation array
            if params.corr_dim > 0:
                xb, yb, xcorrb = datab
                xcorrb = xcorrb.float().to(device)
            # if not use correlation array
            else:
                xb, yb = datab
                xcorrb = None

            # type conversion and move to GPU
            xb = xb.float().to(device)
            if params.num_classes == 2:
                yb = data_utils.label_mixing(yb, 2, 0)  # mix noise (0) and glitch (2) label into (0)
                yb = yb.float().to(device)   # BCELoss takes float tensor
            else:
                yb = yb.view(-1).long().to(device)    # CrossEntropyLoss takes long tensor

            # forward pass and calculate loss
            yhatb = net(xb, xcorrb)
            loss = criterion(yhatb, yb)
            val_loss += loss.item()    # update training loss
            
            # store validation output
            if params.store_val_output:
                train_log.update_predict(yhatb, yb)
            
    # logging at the end of each epoch
    # store average loss per sample
    train_loss /= len(train_loader.dataset)
    val_loss /= len(val_loader.dataset)            
    train_log.update_metric('loss', train_loss, epoch, test_metric=val_loss, 
                            n_batch_total=n_batch_total)
        
    # store metric and model
    train_log.log_metric()
    train_log.save_model(net, epoch)
    train_log.save_optimizer(optimizer, epoch)

    # store val prediction
    if params.store_val_output:
        train_log.save_predict(epoch)
    
    # print out status
    train_log.display_status('loss', train_loss, epoch, test_metric=val_loss, 
                             max_epochs=params.max_epochs, n_batch_total=n_batch_total)
