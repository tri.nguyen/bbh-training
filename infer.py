#!/usr/bin/env python
# coding: utf-8

import os
import sys
import h5py
import argparse 
import logging

import numpy as np
import torch
import torch.nn as nn
from torch.utils.data import DataLoader
from scipy.special import expit

from gwpy.timeseries import TimeSeries

from bbh_classification import classifiers, utils, data_utils

logging.basicConfig(format='%(asctime)s - %(message)s', 
                    level=logging.DEBUG, stream=sys.stdout)


# Convert output to numpy array and calculate statistics
def BCELoss(yhat, y, logit=True):
    if logit:
        yhat = expit(yhat)
    return - np.sum(y * np.log(yhat + 1e-9) + (1- y) * np.log(1 - yhat + 1e-9)) / len(y)
def ACC(yhat, y, logit=True):
    if logit:
        yhat = expit(yhat)
    return np.sum(np.round(yhat)==y) / len(y) * 100

# Parse command line arguments
def parse_cmd():
    parser = argparse.ArgumentParser()
    
    # Input output
    parser.add_argument('--dataset-dir', required=True)
    parser.add_argument('--output', required=True)
    parser.add_argument('--checkpoint', required=None)
    parser.add_argument('--max-checkpoint', type=int, default=15)
    parser.add_argument('--prefix', default='ConvNet')
    parser.add_argument('--input-shape', nargs='+', type=int, default=(2, 1024))

    # Classifier settings
    parser.add_argument('--arch', choices=(classifiers.ALL_NAMES.keys()), 
                        type=str.upper, required=True)
    parser.add_argument('--corr-dim', type=int, default=0)
    parser.add_argument('--corr-key', default='corr')
    parser.add_argument('--padding', type=int, default=1)

    # CUDA
    parser.add_argument('--device', default='cuda')
    
    params = parser.parse_args()
    return params
params = parse_cmd()

# Get device
device = utils.get_device(params.device)

# Create dataset and dataloader
keys = ('data', 'label')
if params.corr_dim > 0:
    keys = ('data', 'label', params.corr_key)
dataset = data_utils.Dataset(params.dataset_dir, keys=keys)
data_loader = DataLoader(dataset, batch_size=128, pin_memory=True, num_workers=4)

logging.info('Number of samples  : {} '.format(len(dataset)))

arch = classifiers.get_arch(params.arch)

# Evaluate dataset
logging.info('Evaluating')
logging.info('Prefix: {}'.format(params.prefix))
if os.path.isdir(params.checkpoint):
    logging.info('Importing all checkpoints from directory: {}'.format(params.checkpoint))
    ckpt_list = []
    prefix_list = []
    for i in range(params.max_checkpoint):
        ckpt = os.path.join(params.checkpoint, 'epoch_{}'.format(i))
        if os.path.exists(ckpt):
            ckpt_list.append(ckpt)
            prefix_list.append('{}-EPOCH-{:02d}'.format(params.prefix, i))
else:
    ckpt_list = [params.checkpoint]
    prefix_list = [params.prefix]

# Loop over all checkpoints
for i in range(len(ckpt_list)):
    ckpt = ckpt_list[i]
    prefix = prefix_list[i]

    # Get NN architecture and model
    logging.info('- Importing checkpoint: {}'.format(ckpt))

    net = arch.Classifier(params.input_shape, params.corr_dim, params.padding)
    net.load_state_dict(torch.load(ckpt, map_location='cpu'))
    net.to(device)
    net.eval()

    # evaluating the validation set
    logit = []
    label = []
    N_data = len(data_loader.dataset)
    with torch.no_grad():
        for i, data_batch in enumerate(data_loader):
            # if include correlation in training
            if params.corr_dim > 0:
                x_batch, y_batch, x_corr = data_batch
                x_corr = x_corr.float().to(device)  
            else:
                x_batch, y_batch = data_batch
                x_corr = None
            label.append(y_batch)

            # type conversion and move to GPU
            x_batch = x_batch.float().to(device)
            y_batch = y_batch.float().to(device)

            # forward pass
            logit_batch = net(x_batch, x_corr=x_corr)
            logit.append(logit_batch.cpu())

    # Calculate BCE loss and accuracy
    label = torch.cat(label).numpy()
    logit = torch.cat(logit).numpy()
    loss = BCELoss(logit, label)
    acc = ACC(logit, label)
    logging.info('- Test Loss /  Accuracy: {:.4f}, {:.2f}%'.format(loss, acc))

    # Write output to file
    with h5py.File(params.output, 'a') as f:
        if prefix is None:
            gr = f
        if f.get(prefix) is not None:
            logging.warning('key {} already existed. Overwrite.'.format(prefix))
            del f[prefix]
            gr = f.create_group(prefix)
            gr.create_dataset('logit', data=logit, chunks=True)
            gr.create_dataset('label', data=label, chunks=True)
        else:
            gr = f.create_group(prefix)
            gr.create_dataset('logit', data=logit, chunks=True)
            gr.create_dataset('label', data=label, chunks=True)
