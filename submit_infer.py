
import os
import errno
import shutil
import sys

import numpy as np

from pycondor import Job, Dagman

# Define HTCondor parameters
job_args = dict(
    submit='condor/submit', log='condor/log',
    error='condor/error', output='condor/output',
    getenv=True,
    universe='vanilla',
    requirements='(CUDACapability > 3.5)',
    notification=None,
    request_memory='2 GB',
    extra_lines= [
        'accounting_group = ligo.dev.o3.cbc.bbh.pycbcoffline',
        'request_gpus = 1',
        'stream_output = True',
        'stream_error = True']
)

# Define Dagman
dagman = Dagman(name='quickDag', submit='condor/submit')
base_dir = '/home/tri.nguyen/bbh/bbh_classification/data/dataset/'
events = (
    'GW170104',
    'GW170729',
    'GW170809',
    'GW170814',
#     'GW170823',
    
)
cnn_epoch = 4
enn_epoch = 19
model = 2

cnn_dir = 'data/TrGW170823-CNNmedium/00/CNN'
enn_dir = 'data/TrGW170823-CNNmedium/00/ENN'
output_dir = 'data/TrGW170823-CNNmedium/00/results/epoch_best'
os.makedirs(output_dir, exist_ok=True)

dag = Dagman('dagGW170823-CNNmedium', submit='data/condor/submit')

for event in events:
    dataset_dir = os.path.join(base_dir, 'Te{}'.format(event))
    if not os.path.exists(dataset_dir):
        raise FileNotFoundError(
            errno.ENOENT, os.strerror(errno.ENOENT), dataset_dir)
    
    output = os.path.join(output_dir, '{}.h5'.format(event))
    
    args = ''
    args += '--dataset-dir {} --output {} '.format(dataset_dir, output)
    args += '--CNN-dir {} --CNN-epoch {} '.format(cnn_dir, cnn_epoch)
#     args += '--ENN-dir {} --ENN-epoch {} '.format(enn_dir, enn_dir)
    args += '--model {} --device cuda'.format(model)

    print(args)
    
    job = Job(name='infer{}'.format(event), dag=dagman, executable='./infer.py',  
              arguments=[args], **job_args)

dagman.build_submit()
