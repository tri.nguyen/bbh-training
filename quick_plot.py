#!/usr/bin/env python
# coding: utf-8

import os
import sys
import argparse
import h5py

import numpy as np
import seaborn as sns
import matplotlib as mpl
import matplotlib.pyplot as plt

mpl.rc('font', size=15)
mpl.rc('figure', figsize=(8, 5), facecolor='white')
colors = plt.rcParams['axes.prop_cycle'].by_key()['color']

import scipy
import scipy.signal as sig
from scipy.special import expit, logit

from bbh_classification.analysis import getROC, getAUC


# Parse CMD argument
def parse_cmd():
    parser = argparse.ArgumentParser()
    parser.add_argument('model_name')
    parser.add_argument('dataset_name')
    params = parser.parse_args()
    return params
params = parse_cmd()

# Define variables
model_name = params.model_name
dataset_name = params.dataset_name
filename = 'data/{}/results/{}.h5'.format(model_name, dataset_name)
plot_dir = 'data/{}/plot/{}/'.format(model_name, dataset_name)

os.makedirs(plot_dir, exist_ok=True)

# Read in input file
parameters = {}
print('Reading {}...'.format(filename))
with h5py.File(filename, 'r') as f:
    for key, val in f.items():
        parameters[key] = val[:]
    parameters.update(dict(f.attrs))

    labels = parameters['y']
    H1_SNR = parameters['H1-SNR']
    L1_SNR = parameters['L1-SNR']
    SNR = np.sqrt(H1_SNR**2 + L1_SNR**2)
    COH_logit = parameters['COH-logit']
    INCOH_logit = parameters['INCOH-logit']
    H1_logit = parameters['H1-logit']
    L1_logit = parameters['L1-logit']

# Calculate sigmoid score from logit
COH_score = expit(COH_logit)
H1_score = expit(H1_logit)
L1_score = expit(L1_logit)
INCOH_score = expit(INCOH_logit)

mnoise = (labels == 0)   # mask noise
msignal = (labels == 1)  # mask signal
SNR[mnoise] = 0  # set all SNR of noise to 0

# Start plotting
# Plot score histogram
fig, (ax1, ax2) = plt.subplots(1, 2, sharey=True, figsize=(12, 5))
bins = np.arange(0., 1.05, 0.05)
sns.distplot(COH_score[mnoise], ax=ax1, bins=bins, hist_kws={'edgecolor': 'k'}, label='Noise')
sns.distplot(COH_score[msignal], ax=ax1, bins=bins, hist_kws={'edgecolor': 'k'}, label='Signal')
sns.distplot(INCOH_score[mnoise], ax=ax2, bins=bins, hist_kws={'edgecolor': 'k'}, label='Noise')
sns.distplot(INCOH_score[msignal], ax=ax2, bins=bins, hist_kws={'edgecolor': 'k'}, label='Signal')
ax1.set_xlabel('Score')
ax2.set_xlabel('Score')
ax1.set_title('COH Network')
ax2.set_title('INCOH Network')
ax1.set_ylabel('PDF')
ax1.legend()
ax2.legend()
fig.tight_layout()
fig.savefig(os.path.join(plot_dir, 'score_histogram.png'), dpi=300, bbox_inches='tight')


# Calculate and plot ROC curve
threshold = np.arange(0., 1.01, 0.001)
# ROC curve
COH_tp, COH_fp = getROC(COH_score[msignal], COH_score[mnoise], threshold)
H1_tp, H1_fp = getROC(H1_score[msignal], H1_score[mnoise], threshold)
L1_tp, L1_fp = getROC(L1_score[msignal], L1_score[mnoise], threshold)
INCOH_tp, INCOH_fp = getROC(INCOH_score[msignal], INCOH_score[mnoise], threshold)
# AUC 
COH_auc = getAUC(COH_tp, COH_fp)
H1_auc = getAUC(H1_tp, H1_fp)
L1_auc = getAUC(L1_tp, L1_fp)
INCOH_auc = getAUC(INCOH_tp, INCOH_fp)

# Plot
fig, ax = plt.subplots()
ax.plot(COH_fp, COH_tp, color='r', label='COH: AUC = {:.4f}'.format(COH_auc))
ax.plot(H1_fp, H1_tp, label='H1: AUC = {:.4f}'.format(H1_auc))
ax.plot(L1_fp, L1_tp, label='L1: AUC = {:.4f}'.format(L1_auc))
ax.plot(INCOH_fp, INCOH_tp, color='k', label='INCOH: AUC = {:.4f}'.format(INCOH_auc))
ax.set(xlabel='False Positive Rate', ylabel='True Positive Rate')
ax.legend()
ax.set_yscale('log')
ax.set_xscale('log')
ax.set_xlim(1e-4, None)
ax.set_ylim(1e-3, None)
fig.tight_layout()
fig.savefig(os.path.join(plot_dir, 'ROC.png'), dpi=300, bbox_inches='tight')

# Calculate score percentile as a function of SNR bin
# Divide SNR into bins
bin_size = 2
SNR_bins = np.arange(0., max(SNR) + bin_size, bin_size)
COH_score_per_bins, INCOH_score_per_bins = [], []
counts = []
for i in range(len(SNR_bins) - 1):
    mask = (SNR_bins[i] <= SNR[msignal]) & (SNR[msignal] < SNR_bins[i + 1])
    COH_score_per_bins.append(COH_score[msignal][mask])
    INCOH_score_per_bins.append(INCOH_score[msignal][mask])
    counts.append(len(COH_score_per_bins[i]))
counts = np.array(counts)

# Plot SNR histogram
fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12, 5))
ax1twin = ax1.twinx()
x = (SNR_bins[:-1] + SNR_bins[1:]) / 2.
ax1.hist(SNR[msignal], bins=SNR_bins, density=True, edgecolor='k')
ax1twin.hist(SNR[msignal], bins=SNR_bins, alpha=0.)
ax1.set_xlabel('SNR')
ax1.set_ylabel('Density')
ax1twin.set_ylabel('Counts')
ax1twin.grid(False)
x = np.arange(0, 40)
ax2.scatter(H1_SNR, L1_SNR)
ax2.plot(x, x, color='k')
ax2.set_xlabel('H1 SNR')
ax2.set_ylabel('L1 SNR')
fig.tight_layout()
fig.savefig(os.path.join(plot_dir, 'SNR.png'), dpi=300, bbox_inches='tight')

# Plot percentile
q = (16, 50, 84)
COH_percentiles, INCOH_percentiles = [], []
for s in COH_score_per_bins:
    if len(s) > 0:
        COH_percentiles.append(np.percentile(s, q=q))
COH_percentiles = np.array(COH_percentiles)
for s in INCOH_score_per_bins:
    if len(s) > 0:
        INCOH_percentiles.append(np.percentile(s, q=q))
INCOH_percentiles = np.array(INCOH_percentiles)
        
fig, (ax1, ax2) = plt.subplots(1, 2, sharey=True, sharex=True, 
                               figsize=(12, 5))
x = (SNR_bins[1:] + SNR_bins[:-1]) / 2.
x = x[counts > 0]
ax1.plot(x, COH_percentiles[:, 1], color='black', ls='-', marker='o', label='Median')
ax1.fill_between(x, COH_percentiles[:, 0], COH_percentiles[:, -1], 
                color='tab:cyan', alpha=0.7, label=r'$2\;\sigma$')
ax2.plot(x, INCOH_percentiles[:, 1], color='black', ls='-', marker='o', label='Median')
ax2.fill_between(x, INCOH_percentiles[:, 0], INCOH_percentiles[:, -1], 
                color='tab:cyan', alpha=0.7, label=r'$2\;\sigma$')
ax1.set(xlabel='Optimal SNR', ylabel='Scores')
ax2.set(xlabel='Optimal SNR')
ax1.legend(loc=4)
ax2.legend(loc=4)
ax1.set_xlim(0, 30)
fig.tight_layout()
fig.savefig(os.path.join(plot_dir, 'SNR_score.png'), dpi=300, bbox_inches='tight')

# Plot sensitivity (True Positive Rate) as a function of SNR
# interpolate 
COH_fp2thres = scipy.interpolate.interp1d(COH_fp, threshold)
INCOH_fp2thres = scipy.interpolate.interp1d(INCOH_fp, threshold)

fars = (0.1, 0.05, 0.001)
COH_thres = COH_fp2thres(fars)
INCOH_thres = INCOH_fp2thres(fars)

COH_sens, INCOH_sens = [], []
for i in range(len(counts)):
    if counts[i] > 0:
        true_pos = np.sum(np.where(COH_score_per_bins[i].reshape(-1, 1) > COH_thres, 1., 0.),  0)
        true_pos /= len(COH_score_per_bins[i])
        COH_sens.append(true_pos)
        
        true_pos = np.sum(np.where(INCOH_score_per_bins[i].reshape(-1, 1) > INCOH_thres, 1., 0.),  0)
        true_pos /= len(INCOH_score_per_bins[i])
        INCOH_sens.append(true_pos)
        
COH_sens = np.array(COH_sens)
INCOH_sens = np.array(INCOH_sens)

# plot true positive prob (sensitivity) as a function of scores
fig, ax = plt.subplots()
x = (SNR_bins[1:] + SNR_bins[:-1]) / 2
x = x[counts > 0]
for i in np.arange(len(fars)):
    ax.plot(x, COH_sens[:, i], marker='o', 
            color=colors[i], label='False Pos= %.1g' % (fars[i]))
    ax.plot(x, INCOH_sens[:, i], marker='v', markersize=10, ls='--', 
            color=colors[i], alpha=0.7)

ax.set(xlabel='Optimal SNR', ylabel='True Positive Rate')
ax.set_xlim(1e-3, 30)
ax.set_ylim(-0.05, 1.05)
ax.set_title('COH (solid) v. INCOH (dashed)')
ax.legend(loc=4)
fig.tight_layout()
fig.savefig(os.path.join(plot_dir, 'SNR_TPR.png'), dpi=300, bbox_inches='tight')
